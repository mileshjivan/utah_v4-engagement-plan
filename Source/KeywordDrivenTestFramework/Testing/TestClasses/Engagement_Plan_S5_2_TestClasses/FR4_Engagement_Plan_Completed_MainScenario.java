/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagement_Plan_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects.Engagement_Plan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR4-Engagement Plan Completed v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Engagement_Plan_Completed_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Engagement_Plan_Completed_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!navigateToEngagements()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Stakeholder Details'.");
    }

    public boolean navigateToEngagements() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Social_Sustainability())) {
            error = "Failed to wait for 'Social Sustainability' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.Social_Sustainability())) {
            error = "Failed to click on 'Social Sustainability' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Social Sustainability' tab.");
        
        //Navigate to Engagement Plan
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.navigate_EngagementPlan())) {
            error = "Failed to wait for 'Engagement Plan' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.navigate_EngagementPlan())) {
            error = "Failed to click on 'Engagements Plan' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements Plan' search page.");

        //Engagement Plan
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlan_Title())){
            error = "Failed to wait for Engagement Plan Title: " + getData("Engagement plan title");
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.engagementPlan_Title(), getData("Engagement plan title"))){
            error = "Failed to enter Engagement Plan Title: " + getData("Engagement plan title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Plan Title: " + getData("Engagement plan title"));
        
        //Frequency
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Frequency_Dropdown())){
            error = "Failed to wait Frequency dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.Frequency_Dropdown())){
            error = "Failed to click the Frequency dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Frequency_Option(getData("Frequency")))){
            error = "Failed to wait for the Frequency Option: " + getData("Frequency");
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.Frequency_Option(getData("Frequency")))){
            error = "Failed to click the Frequency Option: " + getData("Frequency");
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.Frequency_Dropdown())){
            error = "Failed to click the Frequency dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Frequency Option: " + getData("Frequency"));
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.se_search())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.se_search())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EngagementPlan_Record(getData("Frequency")))){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.EngagementPlan_Record(getData("Frequency")))){
                error = "Failed to wait for Engagement plan - Frequency Records: " + getData("Frequency");
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EngagementPlan_Record(getData("Frequency")))){
            error = "Failed to click the Engagement plan - Frequency Records: " + getData("Frequency");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked " + getData("Engagement plan title"));
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlanProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.engagementPlanProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.ValidateByText(Engagement_Plan_PageObjects.Status(), getData("Status"))){
            error = "Failed to validate the Completed Phase. with " + getData("Status");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the process flow phase: " + getData("Status"));
            
        if(getData("Status").equalsIgnoreCase("Planning")){
            //process flow Planning
            if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Planning_Phase())){
                error = "Failed to wait for Planning Phase.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.Planning_Phase())){
                error = "Failed to wait for Planning Phase.";
                return false;
            }
        }
        else if(getData("Status").equalsIgnoreCase("In Progress")){
            //process flow In Progress
            if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.InProgress_Phase())){
                error = "Failed to wait for In Progress Phase.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.InProgress_Phase())){
                error = "Failed to wait for In Progress Phase.";
                return false;
            }
        }
        else if(getData("Status").equalsIgnoreCase("Completed")){
            //process flow Completed
            if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Completed_Phase())){
                error = "Failed to wait for Completed Phase.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.Completed_Phase())){
                error = "Failed to wait for Completed Phase.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully confirmed the status of Engagement Plan is 'Completed'");
        }
        
        return true;
    }

}
