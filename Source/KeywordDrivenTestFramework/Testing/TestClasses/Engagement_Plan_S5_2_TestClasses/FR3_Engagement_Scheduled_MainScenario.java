/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagement_Plan_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects.Engagement_Plan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-Engagement Scheduled v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Engagement_Scheduled_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Engagement_Scheduled_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {

        if (!navigateToEngagements()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Engagement Scheduled - Main scenario'.");
    }

    public boolean navigateToEngagements() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Social_Sustainability())) {
            error = "Failed to wait for 'Social Sustainability' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.Social_Sustainability())) {
            error = "Failed to click on 'Social Sustainability' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Social Sustainability' tab.");
        
        //Navigate to Engagement module
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.navigate_Engagements())) {
            error = "Failed to wait for 'Engagement' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.navigate_Engagements())) {
            error = "Failed to click on 'Engagements' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements' search page.");        
        
        //Engagement date
        if(!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.EngagementStartDate(), getData("Engagement Start Date"))){
            error = "Failed to enter Engagement Start date: " + getData("Engagement Start Date");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Engagement Start date: " + getData("Engagement Start Date"));
        
        //Engagement date
        if(!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.EngagementStartDate(), getData("Engagement End Date"))){
            error = "Failed to enter Engagement End date: " + getData("Engagement End Date");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Engagement End date: " + getData("Engagement End Date"));
        
        //Search Engagements 
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.se_search()))
        {
            error = "Failed to wait for the 'Search' button";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.se_search()))
        {
            error = "Failed to click the 'Search' button";
            return false;
        }
        narrator.stepPassed("Successfully clicked he 'Search' button");
        
        //pause
        SeleniumDriverInstance.pause(900);
        
        narrator.stepPassedWithScreenShot("Successfully viewed all listed Engagements which are created");
        return true;
    }

}
