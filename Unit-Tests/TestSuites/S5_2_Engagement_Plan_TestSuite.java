/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SKhumalo
 */
public class S5_2_Engagement_Plan_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_Engagement_Plan_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;

        //*******************************************
    }

    //S5_2_Engagement_Plan_QA01S5_2
    @Test
    public void S5_2_Engagement_Plan_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Engagement_Plan_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR1-Capture Engagement Plan - Main scenario
    @Test
    public void FR1_Capture_Engagement_Plan_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\FR1-Capture Engagement Plan - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Engagement_Plan_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\FR1-Capture Engagement Plan - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Engagement_Plan_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\FR1-Capture Engagement Plan - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC EDP 0102-Capture Participants - Main Scenario
    @Test
    public void UC_EDP01_02_Capture_Participants_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\UC EDP 01-02-Capture Participants - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC EDP 0103-Update Engagement Status - Main Scenario
    @Test
    public void UC_EDP01_03_Update_Engagement_Status_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\UC EDP 01-03-Update Engagement Status - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void UC_EDP01_03_Update_Engagement_Status_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\UC EDP 01-03-Update Engagement Status - Alternate scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture Engagement Plan Actions - Main Scenario
    @Test
    public void FR2_Capture_Engagement_Plan_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\FR2-Capture Engagement Plan Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR3-Engagement Scheduled - Main scenario
    @Test
    public void FR3_Engagement_Scheduled_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\FR3-Engagement Scheduled - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR4-Engagement Plan Completed - Main scenario
    @Test
    public void FR4_Engagement_Plan_Completed_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagement Plan v5.2\\FR4-Engagement Plan Completed - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
